ARG NODE_VERSION=dubnium-alpine

# Builds a cache layer that can be reused between builds: This layer contains
# both production dependencies and build dependencies that can be pulled
# in a CI environment.
FROM node:${NODE_VERSION} AS cache
ENV NODE_ENV=production
WORKDIR /prod
COPY package-lock.json package.json ./
RUN npm ci --no-optional
WORKDIR /build
ENV NODE_ENV=
COPY package-lock.json package.json ./
RUN npm ci --no-optional

FROM node:${NODE_VERSION} AS build
WORKDIR /build
COPY --from=cache /build/node_modules ./node_modules
COPY . .
RUN npm test
RUN npm run build

FROM node:${NODE_VERSION}
ENV NODE_ENV=production
WORKDIR /app
COPY --from=cache /prod/node_modules ./node_modules
COPY . .
COPY --from=build /build/build ./build

ENV PORT=80
ENTRYPOINT ["npm", "run", "-s"]
CMD ["start"]
EXPOSE 80
