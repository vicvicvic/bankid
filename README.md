# bankid-async

> Google Cloud Function compatible Pub/Sub BankID v5 API

This is a small service which provides an API integration to [BankID]. The service abstracts the underlying BankID functionality to initiate auth and sign requests, and to follow tickets ("orders") to completion.

The service uses Google [Pub/Sub] both to manage the internal state and to publish updates about tickets.

## Features

- Abstraction over BankID auth, signature and cancel APIs (version 5)
- Subscription-based BankID updates, with polling at exactly 2 second intervals
- Parsing of BankID signatures for user data
- No dependencies except for a Pub/Sub system (currently Google Cloud Pub/Sub)
- Cloud function compatible deployment ("serverless")

## Demo application

In the folder `demo/`, there's a `docker-compose` configuration which runs the service and a consumer application using the Google Cloud Pub/Sub emulator in Docker. No setup required!

## Quickstart

This quickstart sets up the required Pub/Sub topic and subscription for the service to accept requests and run orders. You need a Google account in order to use GCP, but it shouldn't cost anything to run Pub/Sub at this scale.

1. Make sure there's a GCP project with Pub/Sub enabled and a topic and subscription setup. If you have `gcloud` installed, you can bootstrap a project with the following:

    ```shell
    # Replace as necessary
    PROJECT=bankid-demo-$RANDOM
    TOPIC=bankid-tickets
    SUBSCRIPTION=bankid-tickets

    # Sets up a new GCP project and enable the Pub/Sub APIs on it
    gcloud projects create $PROJECT
    gcloud services enable --project $PROJECT pubsub.googleapis.com

    # Configure topic and subscription
    gcloud pubsub topics create --project $PROJECT $TOPIC
    gcloud pubsub subscriptions create $SUBSCRIPTION \
      --project $PROJECT \
      --topic $TOPIC
    ```

2. Configure your environment to talk to the right project with the right credentials. For production use, you should probably configure a service account with the following roles:

    - `roles/pubsub.publisher` on the topic (to publish updates)
    - `roles/pubsub.subscriber` on the subscription (to subscribe to updates)

    To create service accounts with these roles, run:

    ```shell
    # Set $PROJECT as above
    SA=bankid-tickets
    SA_EMAIL=$SA@$PROJECT.iam.gserviceaccount.com
    TOPIC=bankid-tickets
    SUBSCRIPTION=bankid-tickets

    # Create service account and setup policy bindings
    gcloud iam service-accounts create --project $PROJECT $SA
    gcloud beta pubsub topics add-iam-policy-binding $TOPIC \
      --project $PROJECT \
      --member=serviceAccount:$SA_EMAIL \
      --role=roles/pubsub.publisher
    gcloud beta pubsub subscriptions add-iam-policy-binding $SUBSCRIPTION \
      --project $PROJECT \
      --member=serviceAccount:$SA_EMAIL \
      --role=roles/pubsub.subscriber

    # And finally, get a key file that can be mounted
    gcloud iam service-accounts keys create iam-account.json --iam-account=$SA_EMAIL
    ```

    For quickstart, you can just configure you application default credentials and make sure the environment points to the right project:

    ```shell
    # Configure Application Default Credentials
    gcloud auth application-default login
    ```

    This will allow the app to talk to Google APIs using Application Default Credentials.

3. Install dependencies and build the app:

    ```shell
    npm install
    npm run build
    ```

4. Run the app with Application Default Credentials and default config:

    ```shell
    npm run start
    ```

## Quickstart with Docker

This service includes a `Dockerfile` to make it easy to build a Docker image. However, when running a Docker container, you absolutely _must_ set up a service account with the appropriate permissions.

The service account key file must be mounted into the Dockerfile and pointed to using `GOOGLE_APPLICATION_CREDENTIALS`. Follow these steps:

1. To create a keyfile from a service account with appropriate permissions, run:

    ```shell
    # PROJECT=as-above
    # TOPIC=bankid-tickets
    # SUBSCRIPTION=bankid-tickets
    SA=bankid-tickets
    SA_EMAIL=$SA@$PROJECT.iam.gserviceaccount.com

    # Create service account and setup policy bindings
    gcloud iam service-accounts create --project $PROJECT $SA
    gcloud beta pubsub topics add-iam-policy-binding $TOPIC \
      --project $PROJECT \
      --member=serviceAccount:$SA_EMAIL \
      --role=roles/pubsub.publisher
    gcloud beta pubsub subscriptions add-iam-policy-binding $SUBSCRIPTION \
      --project $PROJECT \
      --member=serviceAccount:$SA_EMAIL \
      --role=roles/pubsub.subscribe

    # And finally, get a key file that can be mounted
    gcloud iam service-accounts keys create iam-account.json --iam-account=$SA_EMAI
    ```

    The `iam-account.json` file gives access to the service account credentials and should be mounted into the Docker container.

2. Build the Docker image:

    ```shell
    docker build -t bankid
    ```

3. Run the Docker image with appropriate configuration. Here we just mount `$PWD` into `/config` so that we can refer to `iam-account.json`

    ```shell
    docker run \
      -p 8080:8080 \
      -v $PWD:/config \
      -e GOOGLE_APPLICATION_CREDENTIALS=/config/iam-account.json \
      bankid
    ```

## Background

Since BankID released version 5 of its API based on JSON, integrating with the service has no doubt become easier. However, the API is still poll-based, requiring clients to "collect" orders that have been created. Polling should happen at 2 second intervals, and once an order is "complete" it is destroyed.

The purpose of this service is to demonstrate a subscription based approach to handling BankID, without introducing the need for any persistence except the subscription itself. Effectively, once a ticket has been created, the service uses its own subscription to poll BankID at appropriate intervals. Whenever the state of the ticket is updated, an update is published. Once complete, relevant information about the order is published.

## Usage

The service is meant to be deployed as part of a microservice ecosystem. It is not configured with any authentication and is expected to run in a secure environment. Typical usage by another service is as follows:

### Subscribe to `bankid-tickets` in order to receive updates

Whenever the status of a ticket changes, a message is published. Updates are published as soon as a ticket is created. The subscriber should filter based on `type` and `id` of the ticket, in case multiple services are using the same topic.

The data for each ticket is a JSON representation of the attributes. For "complete" tickets, the data also contains "completion data" from BankID (see below).

#### Message attributes

All messages have the following attributes:

| Attribute    | Description                                                         |
| :----------- | :------------------------------------------------------------------ |
| `id`         | Client-provided ID of the ticket. Use this to identify your orders. |
| `type`       | Client-provided type. Use this to identify your orders.             |
| `status`     | Ticket status, one of "pending", "failed" or "complete"             |
| `createTime` | Ticket creation time (ISO 8601 timestamp in UTC)                    |
| `finishTime` | Ticket finish time ("failed" or "complete")                         |
| `hintCode`   | For "pending" and "failed", the BankID "hintCode"                   |

#### Example message when status is "pending"

Attributes:

```json
{
  "id": "665c8573-57ca-48c1-b59e-707616c771a0",
  "type": "FOOBAR",
  "status": "pending",
  "hintCode": "outstandingTransaction",
  "createTime": "2019-01-01T00:00:00.000Z"
}
```

#### Example message when status is "failed"

Attributes:

```json
{
  "id": "665c8573-57ca-48c1-b59e-707616c771a0",
  "type": "FOOBAR",
  "status": "failed",
  "hintCode": "userCancel",
  "createTime": "2019-01-01T00:00:00.000Z",
  "finishTime": "2019-01-01T00:00:01.000Z"
}
```

#### Example message when status is "complete"

Attributes:

```json
{
  "id": "665c8573-57ca-48c1-b59e-707616c771a0",
  "type": "FOOBAR",
  "status": "complete",
  "createTime": "2019-01-01T00:00:00.000Z",
  "finishTime": "2019-01-01T00:00:01.000Z"
}
```

Data:

```json
{
  "id": "665c8573-57ca-48c1-b59e-707616c771a0",
  "type": "FOOBAR",
  "status": "complete",
  "createTime": "2019-01-01T00:00:00.000Z",
  "finishTime": "2019-01-01T00:00:01.000Z",
  "user": {
    "personalNumber": "199001011234",
    "name": "John Smith",
    "givenName": "John",
    "surname": "Smith",
  },
  "device": {
    "ipAddress": "192.168.0.1",
  },
  "cert": {
    "notBefore": "1520031600000",
    "notAfter": "1583189999000"
  },
  "signature": "<?xml version=\"1.0\" ...",
  "ocspResponse": "base64encodedOcspResponse....",
  "userVisibleData": "Hello World!",
  "userNonVisibleData": "SGVsbG8gaW52aXNpYmxlIHdvcmxk"
}
```

Note that `signature` and `userVisibleData` are plain strings, as they are UTF-8 encoded text. However, `ocspResponse` and `userNonVisibleData` are potentially binary and therefore Base64-encoded.

### Initiate an `auth` or `sign` order

When deployed standalone, this service exposes a simple REST API to create `auth` and `sign` orders. These APIs roughly correspond to the BankID v5 equivalents, with the following changes:

- `userVisibleData` is provided in plaintext (it's UTF-8)
- `id` and `type` are supported in order to help identify tickets. These are provided by the client, so that you are ready to receive updates immediately. UUID is recommended for `id`. If a unique topic is used, these attributes may not be necessary.

The endpoints return the `orderRef` and `autoStartToken` from the BankID v5 API. You can use the `orderRef` to `cancel` an order, and the `autoStartToken` to automatically open the user's BankID client.

Example auth request:

```http
POST /auth HTTP/1.1
Content-Type: application/json

{
  "id": "c3bda358-82d8-4e67-8281-15b086608d02",
  "type": "FOOBAR"
}
```

Example sign request:

```http
POST /auth HTTP/1.1
Content-Type: application/json

{
  "id": "c3bda358-82d8-4e67-8281-15b086608d02",
  "type": "FOOBAR",
  "personalNumber": "199001011234",
  "userVisibleData": "Hello world!",
  "userNonVisibleData": "SGVsbG8gQmFzZTY0"
}
```

Example response:

```http
HTTP/1.1 201 OK
Content-Type: application/json

{
  "autoStartToken": "9bd7e107-5ef1-47e9-9212-5eaff2dc0688",
  "orderRef": "614a0426-a592-48ad-bc80-b6d9483d2b35"
}
```


## Configuration

The service contains configuration that can talk to the BankID test API. The app can be configured using some environment variables.

| Name                                 | Description                                                                                                                                                         | Default                   |
| :----------------------------------- | :------------------------------------------------------------------------------------------------------------------------------------------------------------------ | :------------------------ |
| `PORT`                               | Port that Express server listens on                                                                                                                                 | `8080`                    |
| `GOOGLE_APPLICATION_CREDENTIALS`     | Point this to a service account JSON key file. See [Getting Started with Authentication](https://cloud.google.com/docs/authentication/getting-started) for details. |                           |
| `PUBSUB_BANKID_TICKETS_SUBSCRIPTION` | Pub/Sub subscription used to listen for tickets                                                                                                                     | `bankid-tickets`          |
| `PUBSUB_BANKID_TICKETS_TOPIC`        | Pub/Sub topic to publish updates on                                                                                                                                 | `bankid-tickets`          |
| `BANKID_HOST`                        | BankID API host. Set this to `appapi2.bankid.com` in production                                                                                                     | `appapi2.test.bankid.com` |
| `BANKID_CLIENT_PASSPHRASE`           | Passphrase for the .pfx container with BankID client certificate                                                                                                    | `qwerty123`               |

Critically, to in order for the application to talk to the production BankID API, it needs to be configured with the correct host, client certificate and certificate authority. The following files are expected:

| Filename                      | Description                                                                                                              |
| :---------------------------- | :----------------------------------------------------------------------------------------------------------------------- |
| `resources/bankid-ca.pem`     | BankID server's CA certificate. This is available in the RP guidelines, and is bundled as `resources/bankid-ca.prod.pem` |
| `resources/bankid-client.pfx` | BankID client certificate bundle. This is provided by your issuing bank                                                  |


[BankID]: https://www.bankid.com/
[Pub/Sub]: https://cloud.google.com/pubsub/docs/
[Cloud functions]: https://cloud.google.com/functions/docs/
