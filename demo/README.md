# Demo application

> Run a demo application with `docker-compose up`

This is a small demo application which uses the Google Cloud Pub/Sub emulator to showcase how the main service runs, and how a consumer application can be written to listen for updates.

## How to run the demo

1. Configure your client for demo BankID and get a demo BankID from <https://demo.bankid.com/>. Sample requests are setup with personal identity number `19671122-2940`. Of course, you can call the service without a personal number and use autostart tokens instead.

3. Fire up the services. The BankID service will listen on port 8080.

    ```shell
    docker-compose up
    ```

4. Hit the BankID service with a request. There are sample requests in `../sample-requests`:

    ```
    curl -X POST -d @sample-requests/auth-request.json -H "Content-Type: application/json" 0.0.0.0:8080/auth
    ```

5. Sign the request with a demo BankID

6. Watch how the consumer application posts updates about the tickets
