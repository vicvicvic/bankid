// eslint-disable-next-line @typescript-eslint/no-var-requires
const { PubSub } = require("@google-cloud/pubsub");

const pubsub = new PubSub({ projectId: process.env.GOOGLE_CLOUD_PROJECT });

const topic = pubsub.topic("bankid-tickets");
const subscription = topic.subscription("bankid-tickets--consumer");

async function main() {
  await subscription.get({ autoCreate: true });

  subscription.on("message", msg => {
    console.log("Received update about ticket");
    console.log(msg.attributes);
  });
}

main();
