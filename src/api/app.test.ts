const mockConsoleError = jest.fn();
const mockConsoleLog = jest.fn();

const ORDER_REF = "2e975cb1-2540-44c5-a1f3-62fcd4ddafc5";
const AUTO_START_TOKEN = "f334b788-814c-45d2-a74c-3e2585f89e6b";

const mockAuth = jest.fn(async () => ({
  orderRef: ORDER_REF,
  autoStartToken: AUTO_START_TOKEN
}));

const mockSign = jest.fn(async () => ({
  orderRef: ORDER_REF,
  autoStartToken: AUTO_START_TOKEN
}));

const mockCollect = jest.fn(
  async (): Promise<BankIdCollectResponsePending> => ({
    hintCode: "outstandingTransaction",
    orderRef: ORDER_REF,
    status: BankIdStatus.Pending
  })
);

const mockCancel = jest.fn();
const mockGetBankIdClient = jest.fn(() => ({
  cancel: mockCancel,
  auth: mockAuth,
  sign: mockSign,
  collect: mockCollect
}));

jest.mock("../bankid-client/default-client", () => ({
  getBankIdClient: mockGetBankIdClient
}));

const mockBeginCollect = jest.fn();
jest.mock("../pubsub/begin-collect", () => ({
  beginCollect: mockBeginCollect
}));

import request from "supertest";
import { app } from "./app";
import { BankIdError } from "../bankid-client/error";
import {
  BankIdCollectResponsePending,
  BankIdStatus
} from "../bankid-client/types";

beforeEach(() => {
  jest.clearAllMocks();

  console.error = mockConsoleError;
  console.log = mockConsoleLog;
});

const id = "99f2cb5b-203d-496f-b6c3-682c68c64004";
const type = "FOOBAR";

describe("createAuthRequest", () => {
  it("should create auth requests without personalNumber", async () => {
    const res = await request(app)
      .post("/auth")
      .send({ id, type })
      .expect(201);

    expect(mockAuth).toHaveBeenCalledWith({
      personalNumber: undefined,
      endUserIp: expect.any(String)
    });

    expect(res.body).toEqual({
      autoStartToken: AUTO_START_TOKEN,
      orderRef: ORDER_REF
    });
  });

  it("should create auth requests with personalNumber", async () => {
    const personalNumber = "196711222940";

    const res = await request(app)
      .post("/auth")
      .send({ id, type, personalNumber })
      .expect(201);

    expect(mockAuth).toHaveBeenCalledWith({
      personalNumber,
      endUserIp: expect.any(String)
    });

    expect(res.body).toEqual({
      autoStartToken: AUTO_START_TOKEN,
      orderRef: ORDER_REF
    });
  });

  it('should handle "alreadyInProgress" errors', async () => {
    const personalNumber = "196711222940";

    mockAuth.mockImplementationOnce(async () => {
      throw new BankIdError(
        400,
        "alreadyInProgress",
        "Order already in progress for pno"
      );
    });

    const res = await request(app)
      .post("/auth")
      .send({ id, type, personalNumber })
      .expect(400);

    expect(res.body).toEqual({ message: "An order is already in progress" });
  });
});

describe("createSignRequest", () => {
  it("should create sign requests with visible data", async () => {
    const res = await request(app)
      .post("/sign")
      .send({ id, type, userVisibleData: "Hello World!" })
      .expect(201);

    expect(mockSign).toHaveBeenCalledWith({
      personalNumber: undefined,
      endUserIp: expect.any(String),
      userVisibleData: "Hello World!"
    });

    expect(res.body).toEqual({
      autoStartToken: AUTO_START_TOKEN,
      orderRef: ORDER_REF
    });
  });

  it("should create sign requests with non-visible data", async () => {
    const res = await request(app)
      .post("/sign")
      .send({
        id,
        type,
        userVisibleData: "Hello World!",
        userNonVisibleData: Buffer.from("Hello hidden").toString("base64")
      })
      .expect(201);

    expect(mockSign).toHaveBeenCalledWith({
      personalNumber: undefined,
      endUserIp: expect.any(String),
      userVisibleData: "Hello World!",
      userNonVisibleData: Buffer.from("Hello hidden")
    });

    expect(res.body).toEqual({
      autoStartToken: AUTO_START_TOKEN,
      orderRef: ORDER_REF
    });
  });
});

describe("cancel", () => {
  const orderRef = "f1d6a6a1-a2d8-4662-8eda-09fdd0427ee2";

  it.each`
    name                  | body
    ${"empty"}            | ${undefined}
    ${"no orderRef"}      | ${{}}
    ${"invalid orderRef"} | ${{ orderRef: 123 }}
  `("should reject $name", async args => {
    await request(app)
      .post("/cancel")
      .send(args.body)
      .expect(400);

    expect(mockCancel).not.toHaveBeenCalled();
  });

  it("should cancel valid requests", async () => {
    await request(app)
      .post("/cancel")
      .send({ orderRef })
      .expect(204);

    expect(mockCancel).toHaveBeenCalledWith({ orderRef });
  });

  it("should 400 if cancelling with invalid parameters", async () => {
    mockCancel.mockImplementationOnce(async () => {
      throw new BankIdError(400, "invalidParameters", "Order does not exist");
    });

    const res = await request(app)
      .post("/cancel")
      .send({ orderRef })
      .expect(400);

    expect(res.body).toEqual({
      message: "invalidParameters: Order does not exist"
    });
  });
});
