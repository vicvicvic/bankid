import express, { json } from "express";
import { createAuthRequest } from "./create-auth-request";
import { createSignRequest } from "./create-sign-request";
import { cancel } from "./cancel";

/**
 * Express app for standalone deployments
 */
export const app = express();

app.use(json());
app.post("/auth", createAuthRequest);
app.post("/sign", createSignRequest);
app.post("/cancel", cancel);
