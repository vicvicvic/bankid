/* eslint-disable @typescript-eslint/no-explicit-any */
import { createValidate } from "./util/validate";
import { RequestHandler } from "express";
import { asyncMiddleware } from "./util/async-middleware";
import { getBankIdClient } from "../bankid-client/default-client";
import { handleError } from "./util/handle-error";
import { compose } from "compose-middleware";
import { BankIdError } from "../bankid-client/error";
import { ExposedError } from "./util/exposed-error";

export const validate = createValidate({
  title: "Cancel",
  type: "object",
  properties: {
    orderRef: {
      type: "string",
      description: "BankID order ref to cancel"
    }
  },
  required: ["orderRef"]
});

export interface CancelBody {
  orderRef: string;
}

export const handle: RequestHandler = asyncMiddleware(async (req, res) => {
  const { orderRef } = req.body as CancelBody;

  try {
    await getBankIdClient().cancel({ orderRef });
  } catch (err) {
    if (err instanceof BankIdError && err.errorCode === "invalidParameters") {
      throw new ExposedError(err.message, 400);
    }

    throw err;
  }

  return res.status(204).end();
});

/**
 * Handles incoming request for BankID "cancel"
 */
export const cancel = compose([validate, handle, handleError] as any);
