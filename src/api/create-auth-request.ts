/* eslint-disable @typescript-eslint/no-explicit-any */
import { handleError } from "./util/handle-error";
import { compose } from "compose-middleware";
import { createValidate } from "./util/validate";
import { getBankIdClient } from "../bankid-client/default-client";
import { createInitHandler } from "./util/create-init-handler";

export const validate = createValidate({
  title: "AuthRequest",
  type: "object",
  properties: {
    id: {
      type: "string",
      description: "Your ID for tracking the ticket"
    },
    type: {
      type: "string",
      description: "Your ticket type"
    },
    personalNumber: {
      type: "string",
      description: "A Swedish personal identity number"
    }
  },
  required: ["id", "type"]
});

export interface AuthRequestBody {
  id: string;
  type: string;
  personalNumber?: string;
}

export const handle = createInitHandler(
  async (endUserIp: string, body: AuthRequestBody) => {
    const { autoStartToken, orderRef } = await getBankIdClient().auth({
      endUserIp,
      personalNumber: body.personalNumber
    });

    return { autoStartToken, orderRef, id: body.id, type: body.type };
  }
);

/**
 * Handles incoming requests for BankID "auth" operations
 *
 * This function can be deployed as a Cloud function or be mounted into an
 * express app for standalone deployment..
 */
export const createAuthRequest = compose([
  validate,
  handle,
  handleError
] as any);
