/* eslint-disable @typescript-eslint/no-explicit-any */
import { createValidate } from "./util/validate";
import { createInitHandler } from "./util/create-init-handler";
import { getBankIdClient } from "../bankid-client/default-client";
import { handleError } from "./util/handle-error";
import { compose } from "compose-middleware";

export const validate = createValidate({
  title: "SignRequest",
  type: "object",
  properties: {
    id: {
      type: "string",
      description: "Your ID for tracking the ticket"
    },
    type: {
      type: "string",
      description: "Your ticket type"
    },
    personalNumber: {
      type: "string",
      description: "A Swedish personal identity number"
    },
    userVisibleData: {
      type: "string",
      description: "User visible data (in plaintext UTF-8)"
    },
    userNonVisibleData: {
      type: "string",
      contentEncoding: "base64",
      description: "User non-visible data (base64-encoded)"
    }
  },
  required: ["id", "type", "userVisibleData"]
});

export interface SignRequestBody {
  id: string;
  type: string;
  userVisibleData: string;
  userNonVisibleData?: string | null;
  personalNumber?: string;
}

export const handle = createInitHandler(
  async (endUserIp: string, body: SignRequestBody) => {
    const { autoStartToken, orderRef } = await getBankIdClient().sign({
      endUserIp,
      personalNumber: body.personalNumber,
      userVisibleData: body.userVisibleData,
      userNonVisibleData: body.userNonVisibleData
        ? Buffer.from(body.userNonVisibleData, "base64")
        : undefined
    });

    return { autoStartToken, orderRef, id: body.id, type: body.type };
  }
);

/**
 * Handles incoming request for BankID "sign" operations
 *
 * This function can be deployed as a Cloud function or be mounted into an
 * express app for standalone deployment.
 */
export const createSignRequest = compose([
  validate,
  handle,
  handleError
] as any);
