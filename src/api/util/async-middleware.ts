import { RequestHandler } from "express";

/**
 * Helper to wrap async middleware with error-handling
 */
export function asyncMiddleware(middleware: RequestHandler): RequestHandler {
  return async (req, res, next) => {
    try {
      await middleware(req, res, next);
    } catch (err) {
      next(err);
    }
  };
}
