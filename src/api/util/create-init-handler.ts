/* eslint-disable @typescript-eslint/no-explicit-any */

import { asyncMiddleware } from "./async-middleware";
import { BankIdAuthSignResponse } from "../../bankid-client/types";
import { beginCollect } from "../../pubsub/begin-collect";
import { RequestHandler } from "express";
import { formatIp } from "./format-ip";
import { BankIdError } from "../../bankid-client/error";
import { ExposedError } from "./exposed-error";

export interface InitResponse extends BankIdAuthSignResponse {
  id: string;
  type: string;
}

/**
 * Helper function to create different init handlers
 *
 * Auth/sign are effectively the same, with slightly different input
 * requirements.
 */
export const createInitHandler = (
  startRequest: (ip: string, body: any) => Promise<InitResponse>
): RequestHandler => {
  return asyncMiddleware(async (req, res) => {
    const createTime = new Date().toISOString();

    const { autoStartToken, orderRef, id, type } = await startRequest(
      formatIp(req.ip),
      req.body
    ).catch(error => {
      if (error instanceof BankIdError) {
        // Any other error than "alreadyInProgress" should honestly have been
        // caught by us. But "alreadyInProgress" can be sent back to our
        // client.
        if (error.errorCode === "alreadyInProgress") {
          throw new ExposedError("An order is already in progress", 400);
        }
      }

      throw error;
    });

    // We await the initial collect. We don't really have to, but returning a
    // successful response without having tried to collect and publish would
    // signal that the ticket has started working, when it hasn't...
    await beginCollect({ createTime, id, type, orderRef });

    return res.status(201).send({ autoStartToken, orderRef });
  });
};
