/* eslint-disable @typescript-eslint/no-explicit-any */

/**
 * An error that's OK to expose to the world, following Koa conventions
 */
export class ExposedError extends Error {
  /** Koa convention sets this to true for exposed errors */
  public expose: boolean = true;

  /** HTTP status code associated with error, if any */
  public status?: number;

  public constructor(message?: string, status?: number) {
    super(message);

    // For correct appearance in stack traces
    this.name = this.constructor.name;
    this.status = status;
  }

  public format(): any {
    return { message: this.message };
  }
}
