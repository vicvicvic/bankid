import { formatIp } from "./format-ip";

it.each`
  ip                      | expected
  ${"1.2.3.4"}            | ${"1.2.3.4"}
  ${"fd00::0123"}         | ${"fd00::0123"}
  ${"::ffff:10.11.12.13"} | ${"10.11.12.13"}
`("should convert $ip to $expected", args => {
  const actual = formatIp(args.ip);
  expect(actual).toBe(args.expected);
});
