const MAP_PREFIX = "::ffff:";

/**
 * Formats IP addresses to IPv4 or IPv6
 *
 * This is required because BankID does not support IPv6-mapped IPv4 addresses
 * like `::ffff:127.0.0.1`
 */
export function formatIp(ip: string): string {
  if (ip.startsWith(MAP_PREFIX)) {
    return ip.slice(MAP_PREFIX.length);
  }

  return ip;
}
