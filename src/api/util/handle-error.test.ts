/* eslint-disable @typescript-eslint/no-explicit-any */
import { handleError } from "./handle-error";
import { ExposedError } from "./exposed-error";

const mockConsoleError = jest.fn();
const mockConsoleLog = jest.fn();

beforeEach(() => {
  jest.clearAllMocks();

  console.error = mockConsoleError;
  console.log = mockConsoleLog;
});

const createMockRes = () => {
  const res: any = {};
  res.status = jest.fn(() => res);
  res.send = jest.fn(() => res);
  return res;
};

const mockReq = () => ({} as any);

it("forwards if headers sent", () => {
  const next = jest.fn();

  const err = new Error("unknown");
  handleError(err, mockReq(), { headersSent: true } as any, next);
  expect(next).toHaveBeenCalledWith(err);
});

it("should hide internal errors", () => {
  const res = createMockRes();
  const next = jest.fn();

  const message = "unknown";
  handleError(new Error(message), mockReq(), res, next);

  expect(res.status).toHaveBeenCalledWith(500);
  expect(res.send).toHaveBeenCalledWith({ message: "Internal server error" });
  expect(next).not.toHaveBeenCalled();
});

it("should expose exposed errors", () => {
  const res = createMockRes();
  const next = jest.fn();

  const error = new ExposedError("expose me", 400);
  handleError(error, mockReq(), res, next);

  expect(res.status).toHaveBeenCalledWith(error.status);
  expect(res.send).toHaveBeenCalledWith({ message: error.message });
  expect(next).not.toHaveBeenCalled();
});
