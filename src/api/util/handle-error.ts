/* eslint-disable @typescript-eslint/no-explicit-any */
import { ErrorRequestHandler } from "express";
import { ExposedError } from "./exposed-error";

const formatError = (err: Error): any => {
  if ((err as any).format) {
    return (err as any).format();
  }

  const message = (err as ExposedError).expose
    ? err.message
    : "Internal server error";

  return { message };
};

export const handleError: ErrorRequestHandler = (err, _req, res, next) => {
  if (res.headersSent) {
    return next(err);
  }

  const status = (err as any).status || 500;
  const body = formatError(err);

  // If this is not an error that should be exposed, log it as an error.
  // Otherwise, just log it with `console.log`
  const log = (err as any).expose ? console.log : console.error;
  log(err);

  res.status(status).send(body);
};
