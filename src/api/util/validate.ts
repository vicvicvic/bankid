import Ajv, { ErrorObject } from "ajv";
import { JSONSchema7 } from "json-schema";
import { RequestHandler } from "express";
import { ExposedError } from "./exposed-error";

export const ajv = new Ajv();

export class ValidationError extends ExposedError {
  public errors: ErrorObject[];

  public constructor(errors: ErrorObject[] | null | undefined) {
    super("A validation error has occurred", 400);
    this.errors = errors || [];
  }

  public format() {
    return { message: this.message, errors: this.errors };
  }
}

/**
 * Creates Middleware that validates requests according to a JSON schema
 */
export const createValidate = (schema: JSONSchema7): RequestHandler => {
  const v = ajv.compile(schema);

  return (req, res, next) => {
    const valid = v(req.body);
    if (!valid) {
      return next(new ValidationError(v.errors));
    }

    return next();
  };
};
