import { BankIdClient, BANKID_TEST_HOST } from "../bankid-client";
import { join } from "path";
import { readFileSync } from "fs";

let bankIdClient: BankIdClient | undefined;

/**
 * Thunk for getting a BankID client that's configured based on the
 * environment.
 */
export const getBankIdClient = () => {
  if (!bankIdClient) {
    bankIdClient = new BankIdClient({
      host: process.env.BANKID_HOST || BANKID_TEST_HOST,
      passphrase: process.env.BANKID_CLIENT_PASSPHRASE || "qwerty123",
      pfx: readFileSync(join(__dirname, "../../resources/bankid-client.pfx")),
      ca: readFileSync(join(__dirname, "../../resources/bankid-ca.pem"))
    });
  }

  return bankIdClient;
};
