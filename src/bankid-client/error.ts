/**
 * Whitelist of error codes that can be exposed to end users
 */
export const PUBLIC_ERROR_CODES = new Set([
  "alreadyInProgress",
  "requestTimeout"
]);

/**
 * An error corresponding to a well-defined error in section 14.4 of
 * the RP guidelines.
 */
export class BankIdError extends Error {
  /** HTTP status code that BankID responded with */
  public statusCode: number;

  /** Error code defined by BankID */
  public errorCode: string;

  /** Details */
  public details: string;

  public constructor(statusCode: number, errorCode: string, details: string) {
    super(errorCode + ": " + details);

    this.statusCode = statusCode;
    this.errorCode = errorCode;
    this.details = details;
  }
}
