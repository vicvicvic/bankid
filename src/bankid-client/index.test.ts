let mockRequest = jest.fn(async () => ({}));
jest.mock("./request", () => ({ request: mockRequest }));

import { BankIdClient, BANKID_TEST_HOST } from ".";
import {
  INIT_RESPONSE,
  COLLECT_RESPONSE_PENDING,
  ORDER_REF,
  COLLECT_RESPONSE_COMPLETE_AUTH,
  SIGNATURE_AUTH,
  OCSP_RESPONSE,
  COLLECT_RESPONSE_COMPLETE_SIGN,
  SIGNATURE_SIGN,
  USER_VISIBLE_DATA,
  USER_NON_VISIBLE_DATA
} from "./__fixtures__/bankid-responses";
import { BankIdAuthOptions } from "./types";

let client: BankIdClient;

beforeEach(() => {
  mockRequest.mockReset();
  client = new BankIdClient({ host: BANKID_TEST_HOST });
});

afterEach(() => {
  client.destroy();
});

const AUTH_REQUEST: BankIdAuthOptions = {
  endUserIp: "192.168.0.0.1",
  personalNumber: "190000000000"
};

it("should auth", async () => {
  mockRequest.mockImplementationOnce(async () => INIT_RESPONSE);

  const actual = await client.auth(AUTH_REQUEST);
  expect(mockRequest).toHaveBeenCalledTimes(1);
  expect(mockRequest.mock.calls[0].slice(1)).toEqual([
    BANKID_TEST_HOST,
    "auth",
    AUTH_REQUEST
  ]);
  expect(actual).toEqual(INIT_RESPONSE);
});

it("should cancel", async () => {
  mockRequest.mockImplementationOnce(async () => ({}));

  const actual = await client.cancel({ orderRef: ORDER_REF });
  expect(mockRequest).toHaveBeenCalledTimes(1);
  expect(mockRequest.mock.calls[0].slice(1)).toEqual([
    BANKID_TEST_HOST,
    "cancel",
    { orderRef: ORDER_REF }
  ]);
  expect(actual).toEqual({});
});

it("should sign without non-visible data", async () => {
  mockRequest.mockImplementationOnce(async () => INIT_RESPONSE);

  const userVisibleData = "Hello World!";
  const userVisibleDataB64 = Buffer.from(userVisibleData).toString("base64");

  const actual = await client.sign({
    ...AUTH_REQUEST,
    userVisibleData
  });

  expect(mockRequest).toHaveBeenCalledTimes(1);
  expect(mockRequest.mock.calls[0].slice(1)).toEqual([
    BANKID_TEST_HOST,
    "sign",
    { ...AUTH_REQUEST, userVisibleData: userVisibleDataB64 }
  ]);
  expect(actual).toEqual(INIT_RESPONSE);
});

it("should sign with non-visible data", async () => {
  mockRequest.mockImplementationOnce(async () => INIT_RESPONSE);

  const userVisibleData = "Hello World!";
  const userVisibleDataB64 = Buffer.from(userVisibleData).toString("base64");

  const userNonVisibleDataB64 = "SGVsbG8gTW9vbgo=";
  const userNonVisibleData = Buffer.from(userNonVisibleDataB64, "base64");

  const actual = await client.sign({
    ...AUTH_REQUEST,
    userVisibleData,
    userNonVisibleData
  });

  expect(mockRequest).toHaveBeenCalledTimes(1);
  expect(mockRequest.mock.calls[0].slice(1)).toEqual([
    BANKID_TEST_HOST,
    "sign",
    {
      ...AUTH_REQUEST,
      userVisibleData: userVisibleDataB64,
      userNonVisibleData: userNonVisibleDataB64
    }
  ]);
  expect(actual).toEqual(INIT_RESPONSE);
});

it("should collect pending/failed tickets as is", async () => {
  mockRequest.mockImplementationOnce(async () => COLLECT_RESPONSE_PENDING);
  const actual = await client.collect({ orderRef: ORDER_REF });

  expect(mockRequest).toHaveBeenCalledTimes(1);
  expect(mockRequest.mock.calls[0].slice(1)).toEqual([
    BANKID_TEST_HOST,
    "collect",
    {
      orderRef: ORDER_REF
    }
  ]);
  expect(actual).toEqual(COLLECT_RESPONSE_PENDING);
});

it("should collect complete auth", async () => {
  mockRequest.mockImplementationOnce(
    async () => COLLECT_RESPONSE_COMPLETE_AUTH
  );

  const actual = await client.collect({ orderRef: ORDER_REF });

  expect(mockRequest).toHaveBeenCalledTimes(1);
  expect(mockRequest.mock.calls[0].slice(1)).toEqual([
    BANKID_TEST_HOST,
    "collect",
    {
      orderRef: ORDER_REF
    }
  ]);
  expect(actual).toEqual({
    completionData: {
      ...COLLECT_RESPONSE_COMPLETE_AUTH.completionData,
      signature: Buffer.from(SIGNATURE_AUTH, "base64"),
      ocspResponse: OCSP_RESPONSE,
      userVisibleData: null,
      userNonVisibleData: null
    },
    orderRef: ORDER_REF,
    status: "complete"
  });
});

it("should collect complete sign", async () => {
  mockRequest.mockImplementationOnce(
    async () => COLLECT_RESPONSE_COMPLETE_SIGN
  );

  const actual = await client.collect({ orderRef: ORDER_REF });

  expect(mockRequest).toHaveBeenCalledTimes(1);
  expect(mockRequest.mock.calls[0].slice(1)).toEqual([
    BANKID_TEST_HOST,
    "collect",
    {
      orderRef: ORDER_REF
    }
  ]);
  expect(actual).toEqual({
    completionData: {
      ...COLLECT_RESPONSE_COMPLETE_SIGN.completionData,
      signature: Buffer.from(SIGNATURE_SIGN, "base64"),
      ocspResponse: OCSP_RESPONSE,
      userVisibleData: USER_VISIBLE_DATA,
      userNonVisibleData: Buffer.from(USER_NON_VISIBLE_DATA, "base64")
    },
    orderRef: ORDER_REF,
    status: "complete"
  });
});
