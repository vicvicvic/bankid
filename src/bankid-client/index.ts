/* eslint-disable @typescript-eslint/no-explicit-any */
import { Agent, AgentOptions } from "https";
import {
  BankIdAuthSignResponse,
  BankIdAuthOptions,
  BankIdSignOptions,
  BankIdCollectOptions,
  BankIdCollectResponse,
  BankIdCancelOptions,
  BankIdStatus
} from "./types";
import { request } from "./request";
import { parseSignature } from "./parse-signature";

/**
 * Host for test BankID API
 */
export const BANKID_TEST_HOST = "appapi2.test.bankid.com";

/**
 * Host for production BankID API
 *
 * To talk to production BankID, you need to have a client certificate bundle,
 * and trust the CA of BankID.
 */
export const BANKID_HOST = "appapi2.bankid.com";

export interface BankIdClientOptions extends AgentOptions {
  /**
   * BankID service host
   *
   * @example appapi2.test.bankid.com
   */
  host: string;
}

/**
 * A BankID version 5 client
 *
 * This class abstracts the available BankID operations, providing typings,
 * request abstraction and signature parsing over the underlying JSON API.
 *
 * All methods are async and will reject in case of a network issue. If the
 * BankID API responds with a known error message, the methods will reject
 * with a `BankIdError` describing the problem.
 */
export class BankIdClient {
  private agent: Agent;
  private host: string;

  public constructor(options: BankIdClientOptions) {
    const { host, ...agentOptions } = options;

    this.agent = new Agent(agentOptions);
    this.host = host;
  }

  /**
   * Destroys the underlying HTTPS agent
   */
  public destroy(): void {
    this.agent.destroy();
  }

  /**
   * Initiates an `auth` operation
   */
  public async auth(
    options: BankIdAuthOptions
  ): Promise<BankIdAuthSignResponse> {
    return request(this.agent, this.host, "auth", options);
  }

  /**
   * Initiates a `sign` operation
   */
  public async sign(
    options: BankIdSignOptions
  ): Promise<BankIdAuthSignResponse> {
    const { userVisibleData, userNonVisibleData, ...rest } = options;

    const signOptions: any = {
      userVisibleData: Buffer.from(userVisibleData, "utf8").toString("base64"),
      ...rest
    };

    if (userNonVisibleData) {
      signOptions.userNonVisibleData = userNonVisibleData.toString("base64");
    }

    return request(this.agent, this.host, "sign", signOptions);
  }

  /**
   * Collects a BankID ticket
   *
   * When pending or failed, the response is returned as is with a `hintCode`.
   * When complete, the response contains the parsed completion data, including
   * any signed visible or non-visible data.
   */
  public async collect(
    options: BankIdCollectOptions
  ): Promise<BankIdCollectResponse> {
    const res = await request(this.agent, this.host, "collect", options);

    switch (res.status) {
      case BankIdStatus.Complete:
        const { completionData, ...rest } = res;
        const { signature, ocspResponse } = completionData;
        const signatureBuffer = Buffer.from(signature, "base64");

        return {
          completionData: {
            ...completionData,
            ...parseSignature(signatureBuffer),
            signature: signatureBuffer,
            ocspResponse: ocspResponse
          },
          ...rest
        };
      default:
        return res;
    }
  }

  /**
   * Cancels a BankID ticket
   */
  public async cancel(options: BankIdCancelOptions): Promise<void> {
    return request(this.agent, this.host, "cancel", options);
  }
}
