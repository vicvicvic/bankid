import { parseSignature } from "./parse-signature";
import {
  SIGNATURE_SIGN,
  USER_VISIBLE_DATA,
  USER_NON_VISIBLE_DATA,
  SIGNATURE_AUTH
} from "./__fixtures__/bankid-responses";

it("should parse sign signatures", () => {
  const actual = parseSignature(Buffer.from(SIGNATURE_SIGN, "base64"));

  expect(actual).toEqual({
    userVisibleData: USER_VISIBLE_DATA,
    userNonVisibleData: Buffer.from(USER_NON_VISIBLE_DATA, "base64")
  });
});

it("should parse auth signatures", () => {
  const actual = parseSignature(Buffer.from(SIGNATURE_AUTH, "base64"));

  expect(actual).toEqual({
    userVisibleData: null,
    userNonVisibleData: null
  });
});
