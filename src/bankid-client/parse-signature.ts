import xpath from "xpath";
import { DOMParser } from "xmldom";

const parser = new DOMParser();

const select = xpath.useNamespaces({
  bids: "http://www.bankid.com/signature/v1.0.0/types"
});

export interface UserData {
  userVisibleData: string | null;
  userNonVisibleData: Buffer | null;
}

/**
 * Parses user data from a BankID signature
 *
 * Since user visible data _must_ be UTF-8 plaintext, we return that as a
 * string, but user non-visible data as a `Buffer` since it can be binary.
 */
export function parseSignature(source: Buffer): UserData {
  const doc = parser.parseFromString(source.toString("utf8"));
  const node = select("//bids:bankIdSignedData", doc, true) as Node | undefined;

  if (!node) {
    throw new RangeError("Cannot find bankIdSignedData in signature");
  }

  const visible = select("string(bids:usrVisibleData)", node, true) as string;
  const nonVisible = select(
    "string(bids:usrNonVisibleData)",
    node,
    true
  ) as string;

  return {
    userVisibleData: visible ? Buffer.from(visible, "base64").toString() : null,
    userNonVisibleData: nonVisible ? Buffer.from(nonVisible, "base64") : null
  };
}
