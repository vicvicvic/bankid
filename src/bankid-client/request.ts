/* eslint-disable @typescript-eslint/no-explicit-any */

import { Agent, request as httpsRequest } from "https";
import { BankIdError } from "./error";

/**
 * Do a request to the BankID RPC API
 *
 * This function is a small abstraction over the `https.request` function, to
 * hide the callback API and support mocking (so this module can be mocked
 * instead of that entire module).
 *
 * This function only handles content types and errors that BankID is supposed
 * to return. If the API returns with an error, this function rejects with a
 * `BankIdError`; any other errors are rejected "as-is".
 */
export function request<T = any>(
  agent: Agent,
  hostname: string,
  method: string,
  data: any
): Promise<T> {
  const info = {
    hostname,
    agent,
    path: "/rp/v5/" + method,
    method: "POST",
    headers: {
      "Content-Type": "application/json"
    }
  };

  return new Promise<T>((resolve, reject) => {
    const req = httpsRequest(info, res => {
      res.setEncoding("utf8");

      let data = "";
      res.on("data", chunk => {
        data += chunk;
      });

      res.on("end", () => {
        const body = JSON.parse(data);

        if (body.errorCode) {
          const err = new BankIdError(
            res.statusCode || 500,
            body.errorCode,
            body.details
          );

          return reject(err);
        }

        return resolve(body);
      });
    });

    req.on("err", reject);
    req.write(JSON.stringify(data));
    req.end();
  });
}
