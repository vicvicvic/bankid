//#region BankID auth and sign

export interface BankIdAuthOptions {
  personalNumber?: string;
  endUserIp: string;
}

export interface BankIdSignOptions extends BankIdAuthOptions {
  /** Text displayed for user when signing */
  userVisibleData: string;

  /** Data not visible to user */
  userNonVisibleData?: Buffer;
}

export interface BankIdAuthSignResponse {
  autoStartToken: string;
  orderRef: string;
}

//#endregion

//#region BankID cancel

export interface BankIdCancelOptions {
  orderRef: string;
}

//#endregion

//#region BankID collect

export enum BankIdStatus {
  Pending = "pending",
  Failed = "failed",
  Complete = "complete"
}

export interface BankIdCollectOptions {
  orderRef: string;
}

export type BankIdCollectResponse =
  | BankIdCollectResponsePending
  | BankIdCollectResponseFailed
  | BankIdCollectResponseComplete;

export interface BankIdCollectResponsePending {
  orderRef: string;
  status: BankIdStatus.Pending;
  hintCode: string;
}

export interface BankIdCollectResponseFailed {
  orderRef: string;
  status: BankIdStatus.Failed;
  hintCode: string;
}

export interface BankIdCollectResponseComplete {
  orderRef: string;
  status: BankIdStatus.Complete;
  completionData: BankIdCompletionData;
}

/**
 * Completion data, as documented in section 14.2.5 of BankID relying party
 * guidelines, with the following changes:
 *
 * - `signature` is a Buffer of the base64-decoded XML signature
 * - `ocspResponse` is the Base64-encoded OCSP response
 * - `userVisibleData` is the user visible data, if applicable
 * - `userNonVisibleData` is the user non-visible data, if applicable
 */
export interface BankIdCompletionData {
  user: User;
  device: Device;
  cert: Cert;
  signature: Buffer;
  ocspResponse: string;
  userVisibleData: string | null;
  userNonVisibleData: Buffer | null;
}

export interface User {
  personalNumber: string;
  name: string;
  givenName: string;
  surname: string;
}

export interface Device {
  ipAddress: string;
}

export interface Cert {
  /** First validity as a UNIX timestamp in milliseconds */
  notBefore: string;

  /** Final validity as a UNIX timestamp in milliseconds */
  notAfter: string;
}

//#endregion
