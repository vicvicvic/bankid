const mockGetBankIdClient = jest.fn();
const mockPublishTicket = jest.fn();

jest.mock("../bankid-client/default-client", () => ({
  getBankIdClient: mockGetBankIdClient
}));

jest.mock("./publish-ticket", () => ({ publishTicket: mockPublishTicket }));

import {
  BankIdCollectResponsePending,
  BankIdStatus,
  BankIdCollectResponseFailed
} from "../bankid-client/types";
import { ORDER_REF } from "../bankid-client/__fixtures__/bankid-responses";
import { beginCollect } from "./begin-collect";

const initTicket = {
  createTime: "2019-01-01T00:00:00.000Z",
  id: "54aaa67f-efe6-41b6-9cd3-6ecc23bd5be2",
  orderRef: ORDER_REF,
  type: "FOOBAR"
};

it("should begin collecting", async () => {
  const response: BankIdCollectResponsePending = {
    status: BankIdStatus.Pending,
    hintCode: "outstandingTransaction",
    orderRef: ORDER_REF
  };

  mockGetBankIdClient.mockImplementationOnce(() => ({
    collect: async () => response
  }));

  await beginCollect(initTicket);

  expect(mockPublishTicket).toHaveBeenLastCalledWith({
    createTime: initTicket.createTime,
    id: initTicket.id,
    type: initTicket.type,
    orderRef: initTicket.orderRef,
    status: response.status,
    hintCode: response.hintCode
  });
});

it("should publish a failed ticket immediately", async () => {
  const response: BankIdCollectResponseFailed = {
    status: BankIdStatus.Failed,
    hintCode: "userCancel",
    orderRef: ORDER_REF
  };

  mockGetBankIdClient.mockImplementationOnce(() => ({
    collect: async () => response
  }));

  await beginCollect(initTicket);

  expect(mockPublishTicket).toHaveBeenLastCalledWith({
    createTime: initTicket.createTime,
    id: initTicket.id,
    type: initTicket.type,
    orderRef: initTicket.orderRef,
    status: response.status,
    hintCode: response.hintCode,
    finishTime: expect.any(String)
  });
});
