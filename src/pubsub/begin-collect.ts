import { getBankIdClient } from "../bankid-client/default-client";
import { publishTicket } from "./publish-ticket";
import { TicketAttributesBase } from "./types";
import { BankIdStatus } from "../bankid-client/types";
import { reduceCollectResponse } from "./reduce-collect-response";

/**
 * Starts collecting and publishing updates on a new ticket
 */
export async function beginCollect(
  initTicket: Pick<
    TicketAttributesBase,
    "createTime" | "id" | "orderRef" | "type"
  >
) {
  // Collect the ticket once
  const res = await getBankIdClient().collect({
    orderRef: initTicket.orderRef
  });

  // Reduce the ticket. We have to use a "dummy hint code" to be consistent
  // with the typings. The reducer function could be refactored to accept
  // this initial state. We need to pass `res` through the reduction function,
  // in case the ticket failed or completed directly
  const ticket = reduceCollectResponse(
    {
      ...initTicket,
      status: BankIdStatus.Pending,
      hintCode: DUMMY_HINT_CODE
    },
    res
  );

  await publishTicket(ticket);
}

const DUMMY_HINT_CODE = "";
