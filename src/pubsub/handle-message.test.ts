/* eslint-disable @typescript-eslint/no-explicit-any */

const mockConsoleError = jest.fn();
const mockConsoleLog = jest.fn();
const mockPublishTicket = jest.fn();
const mockGetBankIdClient = jest.fn();

jest.mock("../bankid-client/default-client", () => ({
  getBankIdClient: mockGetBankIdClient
}));

jest.mock("./publish-ticket", () => ({ publishTicket: mockPublishTicket }));

import { Message } from "@google-cloud/pubsub";
import { PreciseDate } from "@google-cloud/precise-date";
import { handleMessage } from "./handle-message";
import {
  BankIdStatus,
  BankIdCollectResponsePending
} from "../bankid-client/types";
import { ORDER_REF } from "../bankid-client/__fixtures__/bankid-responses";

beforeEach(() => {
  jest.useFakeTimers();
  jest.clearAllMocks();

  console.error = mockConsoleError;
  console.log = mockConsoleLog;
});

const createMessage = (attributes: any, data?: Buffer): Message =>
  ({
    id: "772c5d95-19e8-485d-89f6-18d55278a697",
    ackId: "94636782-2abe-4d31-bd4c-24b7872fd701",
    publishTime: new PreciseDate("2019-01-01T00:00:00.000Z"),
    ack: jest.fn(),
    nack: jest.fn(),
    attributes,
    data: data ? data : Buffer.from("")
  } as any);

it("should validate tickets", async () => {
  const msg = createMessage({});
  await handleMessage(msg);

  expect(mockConsoleError).toHaveBeenCalled();
  expect(msg.ack).toHaveBeenCalled();
});

it("should ack failed tickets", async () => {
  const msg = createMessage({ status: BankIdStatus.Failed });
  await handleMessage(msg);

  expect(mockConsoleError).not.toHaveBeenCalled();
  expect(msg.ack).toHaveBeenCalled();
});

it("should ack failed tickets", async () => {
  const msg = createMessage({ status: BankIdStatus.Complete });
  await handleMessage(msg);

  expect(mockConsoleError).not.toHaveBeenCalled();
  expect(msg.ack).toHaveBeenCalled();
});

it("should not publish updates if ticket hasn't changed", async () => {
  const msg = createMessage({
    status: BankIdStatus.Pending,
    hintCode: "outstandingTransaction",
    orderRef: ORDER_REF
  });

  const response: BankIdCollectResponsePending = {
    status: BankIdStatus.Pending,
    hintCode: "outstandingTransaction",
    orderRef: ORDER_REF
  };

  mockGetBankIdClient.mockImplementationOnce(() => ({
    collect: async () => response
  }));

  const p = handleMessage(msg);
  jest.runAllTimers();
  await p;

  expect(setTimeout).toHaveBeenLastCalledWith(expect.any(Function), 2000);
  expect(mockPublishTicket).not.toHaveBeenCalled();
});

it("should publish updates if ticket has changed", async () => {
  const msg = createMessage({
    status: BankIdStatus.Pending,
    hintCode: "outstandingTransaction",
    orderRef: ORDER_REF
  });

  const response: BankIdCollectResponsePending = {
    status: BankIdStatus.Pending,
    hintCode: "userSign",
    orderRef: ORDER_REF
  };

  mockGetBankIdClient.mockImplementationOnce(() => ({
    collect: async () => response
  }));

  const p = handleMessage(msg);
  jest.runAllTimers();
  await p;

  expect(setTimeout).toHaveBeenLastCalledWith(expect.any(Function), 2000);
  expect(mockPublishTicket).toHaveBeenCalled();
});
