import { Message } from "@google-cloud/pubsub";
import { isPending, TicketAttributes } from "./types";
import { getBankIdClient } from "../bankid-client/default-client";
import { reduceCollectResponse } from "./reduce-collect-response";
import { publishTicket } from "./publish-ticket";

/** BankID allows us to poll every 2000 milliseconds */
const BANKID_POLL_INTERVAL = 2000;

/**
 * Handles BankID messages by polling for them if necessary
 *
 * If we've received an in-progress ticket, we wait for a while before polling
 * for it. If the poll results in an updated ticket, we publish an update.
 * Otherwise, we don't publish anything and simply nack the message, causing
 * us to poll again later.
 */
export async function handleMessage(msg: Message) {
  const ticket = msg.attributes as TicketAttributes;

  // Validate that this is a valid ticket
  const ok = ticket.status;
  if (!ok) {
    console.error(new Error(`Invalid ticket: ${ticket}`));
    return msg.ack();
  }

  // If the ticket isn't pending, there's isn't anything for us to do. We
  // receive this message once we've published that we're done with the
  // ticket.
  if (!isPending(ticket)) {
    return msg.ack();
  }

  // We collect the ticket and reduce the response to a new ticket. If the
  // ticket has changed, we publish an update. Otherwise, we nack the message
  // so that we can poll it again.
  const poll = async () => {
    try {
      const res = await getBankIdClient().collect({
        orderRef: ticket.orderRef
      });

      const newTicket = reduceCollectResponse(ticket, res);

      // Ticket has not changed, nack the message so that we will be told
      // about it again...
      if (newTicket === ticket) {
        return msg.nack();
      }

      // Otherwise, we have a new ticket that we want to publish an update
      // about
      await publishTicket(newTicket);
      console.log(`Updated ${newTicket.id} to ${newTicket.status}`);

      return msg.ack();
    } catch (err) {
      // It's safest to `ack` this message, because it's unlikely that this
      // is an error we can recover from.
      msg.ack();

      err.message = `Failed to update ticket: ` + err.message;
      console.error(err);
    }
  };

  // In production, no one will actually await us. However, for testing, it's
  // convenient to put this into a Promise.
  return wait(BANKID_POLL_INTERVAL).then(poll);
}

const wait = (ms: number) => new Promise(resolve => setTimeout(resolve, ms));
