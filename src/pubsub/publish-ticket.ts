/* eslint-disable @typescript-eslint/no-object-literal-type-assertion */
import {
  TicketAttributes,
  TicketData,
  TicketAttributesComplete,
  TicketAttributesFailed,
  TicketAttributesPending
} from "./types";
import { topic } from "./pubsub-client";
import { BankIdStatus } from "../bankid-client/types";
import { Attributes } from "@google-cloud/pubsub";

export async function publishTicket(ticket: TicketData) {
  const buffer = Buffer.from(JSON.stringify(ticket));
  const attributes = getAttributes(ticket);

  // Typecase here because Pub/Sub types are... messy
  await topic.publish(buffer, (attributes as unknown) as Attributes);
}

const getAttributes = (ticket: TicketData): TicketAttributes => {
  switch (ticket.status) {
    case BankIdStatus.Complete:
    case BankIdStatus.Failed:
      return {
        id: ticket.id,
        type: ticket.type,
        status: ticket.status,
        createTime: ticket.createTime,
        orderRef: ticket.orderRef,
        finishTime: ticket.finishTime
      } as TicketAttributesComplete | TicketAttributesFailed;
    default:
      return {
        id: ticket.id,
        type: ticket.type,
        createTime: ticket.createTime,
        orderRef: ticket.orderRef,
        status: ticket.status,
        hintCode: ticket.hintCode
      } as TicketAttributesPending;
  }
};
