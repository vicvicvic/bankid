/**
 * Provides the Pub/Sub client
 *
 * The topic is always required, in order to publish updates. However, the
 * subscriber can be deployed as a Cloud background function.
 *
 * It is expected that Pub/Sub is configured implicitly (by running on GCP),
 * or by using `GOOGLE_APPLICATION_CREDENTIALS` pointing to a keyfile.
 */

import { PubSub } from "@google-cloud/pubsub";

export let pubsub: PubSub;

// XXX: Google says that they support this environment variable, but they
// don't, so, we set it ourselves.
if (process.env.GOOGLE_CLOUD_PROJECT) {
  pubsub = new PubSub({ projectId: process.env.GOOGLE_CLOUD_PROJECT });
} else {
  pubsub = new PubSub();
}

export const PUBSUB_BANKID_TICKETS_SUBSCRIPTION =
  process.env.PUBSUB_BANKID_TICKETS_SUBSCRIPTION || "bankid-tickets";

export const PUBSUB_BANKID_TICKETS_TOPIC =
  process.env.PUBSUB_BANKID_TICKETS_TOPIC || "bankid-tickets";

export const topic = pubsub.topic(PUBSUB_BANKID_TICKETS_TOPIC);
