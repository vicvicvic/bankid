import {
  TicketAttributesPending,
  TicketAttributesFailed,
  TicketDataComplete
} from "./types";
import {
  BankIdStatus,
  BankIdCollectResponsePending,
  BankIdCollectResponseFailed,
  BankIdCollectResponseComplete
} from "../bankid-client/types";
import { reduceCollectResponse } from "./reduce-collect-response";
import {
  ORDER_REF,
  COMPLETION_DATA,
  SIGNATURE_SIGN,
  USER_NON_VISIBLE_DATA,
  USER_VISIBLE_DATA,
  OCSP_RESPONSE
} from "../bankid-client/__fixtures__/bankid-responses";

const TICKET_PENDING: TicketAttributesPending = {
  id: "8f3eeb71-d1f7-4a69-978e-d40c51749b35",
  orderRef: ORDER_REF,
  createTime: "2019-01-01T00:00:00Z",
  type: "foobar",
  status: BankIdStatus.Pending,
  hintCode: "foobar"
};

it("should return identity for same ticket", () => {
  const res: BankIdCollectResponsePending = {
    hintCode: TICKET_PENDING.hintCode,
    orderRef: TICKET_PENDING.orderRef,
    status: BankIdStatus.Pending
  };

  const actual = reduceCollectResponse(TICKET_PENDING, res);
  expect(actual).toBe(TICKET_PENDING);
});

it("should reduce new pending hint code", () => {
  const res: BankIdCollectResponsePending = {
    hintCode: "foobaz",
    orderRef: TICKET_PENDING.orderRef,
    status: BankIdStatus.Pending
  };

  const actual = reduceCollectResponse(TICKET_PENDING, res);
  expect(actual).toEqual({ ...TICKET_PENDING, hintCode: res.hintCode });
});

it("should reduce failed status", () => {
  const res: BankIdCollectResponseFailed = {
    hintCode: "failure",
    orderRef: TICKET_PENDING.orderRef,
    status: BankIdStatus.Failed
  };

  const actual = reduceCollectResponse(
    TICKET_PENDING,
    res
  ) as TicketAttributesFailed;

  expect(actual).toEqual({
    ...TICKET_PENDING,
    status: BankIdStatus.Failed,
    hintCode: res.hintCode,
    finishTime: expect.any(String)
  });

  expect(Date.parse(actual.finishTime)).not.toBeNaN();
});

it("should reduce complete ticket", () => {
  const res: BankIdCollectResponseComplete = {
    completionData: {
      ...COMPLETION_DATA,
      signature: Buffer.from(SIGNATURE_SIGN, "base64"),
      ocspResponse: OCSP_RESPONSE,
      userNonVisibleData: Buffer.from(USER_NON_VISIBLE_DATA, "base64"),
      userVisibleData: USER_VISIBLE_DATA
    },
    orderRef: TICKET_PENDING.orderRef,
    status: BankIdStatus.Complete
  };

  const actual = reduceCollectResponse(
    TICKET_PENDING,
    res
  ) as TicketDataComplete;

  expect(actual).toEqual({
    createTime: TICKET_PENDING.createTime,
    finishTime: expect.any(String),
    id: TICKET_PENDING.id,
    signature: Buffer.from(SIGNATURE_SIGN, "base64").toString("utf8"),
    ocspResponse: OCSP_RESPONSE,
    user: COMPLETION_DATA.user,
    device: COMPLETION_DATA.device,
    cert: COMPLETION_DATA.cert,
    userNonVisibleData: USER_NON_VISIBLE_DATA,
    userVisibleData: USER_VISIBLE_DATA,
    orderRef: TICKET_PENDING.orderRef,
    status: BankIdStatus.Complete,
    type: TICKET_PENDING.type
  });
});
