import { TicketAttributesPending, TicketData } from "./types";
import { BankIdStatus, BankIdCollectResponse } from "../bankid-client/types";

/**
 * Reduces existing ticket and a new collect response to a new ticket
 *
 * We only ever reduce pending tickets (because they're the only once worth
 * publishing updates about). If a collect response has the same hint code as
 * the existing pending ticket, we return the original ticket.
 */
export function reduceCollectResponse(
  ticket: TicketAttributesPending,
  res: BankIdCollectResponse
): TicketData {
  switch (res.status) {
    case BankIdStatus.Complete:
      // eslint-disable-next-line @typescript-eslint/no-unused-vars
      const { hintCode, ...oldTicket } = ticket;
      const {
        signature,
        userNonVisibleData,
        ...completionData
      } = res.completionData;

      return {
        ...oldTicket,
        ...completionData,
        status: BankIdStatus.Complete,
        finishTime: new Date().toISOString(),
        signature: signature.toString("utf8"),
        userNonVisibleData: userNonVisibleData
          ? userNonVisibleData.toString("base64")
          : null
      };
    case BankIdStatus.Failed:
      return {
        ...ticket,
        hintCode: res.hintCode,
        status: BankIdStatus.Failed,
        finishTime: new Date().toISOString()
      };
    default:
      // Ticket did not change; return original ticket
      if (res.hintCode === ticket.hintCode) {
        return ticket;
      }

      return {
        ...ticket,
        hintCode: res.hintCode
      };
  }
}
