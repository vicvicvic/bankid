/**
 * This module contains interface definitions for ticket attributes and data
 *
 * Google Cloud Pub/Sub supports messages having attributes as key-value
 * pairs, and blobs as data. We use attributes of the messages to determine
 * what actions to take, and to avoid decoding data -- where we only put the
 * big payloads for completion.
 */

import { BankIdStatus, User, Device, Cert } from "../bankid-client/types";

//#region Ticket attributes data types

/**
 * Type used for Pub/Sub ticket attributes
 */
export type TicketAttributes =
  | TicketAttributesPending
  | TicketAttributesFailed
  | TicketAttributesComplete;

export interface TicketAttributesBase {
  /** User-assigned ID for tracking this ticket */
  id: string;

  /** Service-specific categorization, for quick message filtering */
  type: string;

  /** Ticket status */
  status: BankIdStatus;

  /** Time that the ticket was created for us (ISO 8601) */
  createTime: string;

  /** BankID order reference */
  orderRef: string;
}

export interface TicketAttributesComplete extends TicketAttributesBase {
  status: BankIdStatus.Complete;

  /** ISO-8601 timestamp of when ticket finished */
  finishTime: string;
}

export interface TicketAttributesFailed extends TicketAttributesBase {
  status: BankIdStatus.Failed;
  hintCode: string;

  /** ISO-8601 timestamp of when ticket finished */
  finishTime: string;
}

export interface TicketAttributesPending extends TicketAttributesBase {
  status: BankIdStatus.Pending;
  hintCode: string;
}

export function isPending(x: TicketAttributes): x is TicketAttributesPending {
  return x.status === BankIdStatus.Pending;
}

//#endregion

//#region Ticket data types

export interface TicketDataComplete extends TicketAttributesComplete {
  user: User;
  device: Device;
  cert: Cert;

  /** XML signature (UTF-8 encoded) */
  signature: string;

  /** OCSP response (Base64-encoded) */
  ocspResponse: string;

  /** User visible data, if applicable (UTF-8 encoded) */
  userVisibleData: string | null;

  /** User non-visible data, if applicable (Base64-encoded) */
  userNonVisibleData: string | null;
}

export type TicketDataPending = TicketAttributesPending;
export type TicketDataFailed = TicketAttributesFailed;

export type TicketData =
  | TicketDataPending
  | TicketDataFailed
  | TicketDataComplete;

//#endregion
