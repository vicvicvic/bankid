import { app } from "./api/app";
import {
  PUBSUB_BANKID_TICKETS_SUBSCRIPTION,
  topic
} from "./pubsub/pubsub-client";
import { handleMessage } from "./pubsub/handle-message";

/**
 * Application main function when running as a standalone service
 *
 * This function starts an express server and initializes the Pub/Sub message
 * handler to listen for messages.
 */
async function main() {
  // Start Express server to listen for incoming requests
  const port = process.env.PORT || 8080;
  app.listen(port, () => {
    console.log(`Server is up on ${port}`);
  });

  // Ensure topic and subscription exists
  await topic.get({ autoCreate: true });
  const subscription = topic.subscription(PUBSUB_BANKID_TICKETS_SUBSCRIPTION);
  await subscription.get({ autoCreate: true });

  subscription.on("error", err => {
    console.error("Pub/Sub error: ", err);
    process.exit(1);
  });

  subscription.on("message", handleMessage);
  console.log(`Listening for BankID tickets on ${subscription.name}`);
}

if (require.main === module) {
  main();
}
